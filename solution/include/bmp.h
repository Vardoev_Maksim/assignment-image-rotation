//
// Created by Maxim on 29.12.2021.
//

#ifndef BMP
#define BMP


#pragma pack(push, 1)
struct header {
    uint16_t bfType; // 0x4d42 | 0x4349 | 0x5450
    uint32_t bfileSize; // размер файла
    uint32_t bfReserved; // 0
    uint32_t bOffBits; // смещение до поля данных,
    uint32_t biSize; // размер структуры в байтах: 40(BITMAPINFOHEADER) или 108(BITMAPV4HEADER) или 124(BITMAPV5HEADER)
    uint32_t biWidth; // ширина в точках
    uint32_t biHeight; // высота в точках
    uint16_t biPlanes; // всегда 1
    uint16_t biBitCount; // 0 | 1 | 4 | 8 | 16 | 24 | 32
    uint32_t biCompression; // BI_RGB | BI_RLE8 | BI_RLE4 | BI_BITFIELDS | BI_JPEG | BI_PNG реально используется лишь BI_RGB
    uint32_t biSizeImage; // Количество байт в поле данных (обычно 0)
    uint32_t biXPelsPerMeter; // горизонтальное разрешение, точек на дюйм
    uint32_t biYPelsPerMeter; // вертикальное разрешение, точек на дюйм
    uint32_t biClrUsed; // Количество используемых цветов
    uint32_t biClrImportant; // Количество существенных цветов (можно считать 0)
};
#pragma pack(pop)
enum bmp_write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_INPUT
};

enum bmp_read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_SIZE,
};



enum bmp_read_status from_bmp(FILE* in, struct image* img);
enum bmp_write_status to_bmp(FILE* out, struct image const* img);

enum bmp_read_status print_ReadError(enum bmp_read_status status);
enum bmp_write_status print_WriteError(enum bmp_write_status status);

#endif //BMP

