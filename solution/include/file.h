//
// Created by Maxim on 29.12.2021.
//
#pragma once
#include <stdio.h>
#ifndef FILE_H
#define FILE_H

enum bmp_Read_Status{
    FILE_READ_OK = 0,
    FILE_READ_ERROR
};

enum bmp_Write_Status{
    FILE_WRITE_OK = 0,
    FILE_WRITE_ERROR
};

enum bmp_Close_Status{
    FILE_CLOSE_OK = 0,
    FILE_CLOSE_ERROR
};

enum bmp_Read_Status read_bmp (FILE** file, const char* path);
enum bmp_Read_Status print_bmpReadStatus(enum bmp_Read_Status status);

enum bmp_Write_Status write_bmp (FILE** file, const char* path);
enum bmp_Write_Status print_bmpWriteStatus(enum bmp_Write_Status status);

enum bmp_Close_Status close_bmp (FILE** file);
enum bmp_Close_Status print_file_close_image_status(enum bmp_Close_Status status);

#endif //FILE_H


