#ifndef IMAGE_H
#define IMAGE_H
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

struct pixel {
    uint8_t b, g, r;
};

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

void initialize_img(struct image* img,uint64_t width, uint64_t height );

void free_d(struct image *img);

#endif //IMAGE_H

