#ifndef ROTATION_H
#define ROTATION_H

#include "image.h"

struct image rotate(struct image const* image);


#endif //ROTATION_H

