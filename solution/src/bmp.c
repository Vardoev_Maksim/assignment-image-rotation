//
// Created by Maxim on 29.12.2021.
//

#include "image.h"
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

#define TYPE 0x4d42
#define RESERVED 0
#define SIZE 40
#define PLANES 1
#define BITCOUNT 24
#define COMPRESSION 0
#define XPPM 0
#define YPPM 0
#define CLRUSET 0
#define IMPORTANT 0

uint32_t padding(struct image const *img){
    return (uint32_t)img->width%4;
}

static struct header bmp_header_set(const struct image* img) {
    struct header Header = {
            .bfType = TYPE,
            .bfileSize = img->width * img->height * sizeof(struct pixel) + img->height * padding(img) + sizeof(struct header),
            .bfReserved = RESERVED,
            .bOffBits = sizeof(struct header),
            .biSize = SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BITCOUNT,
            .biCompression = COMPRESSION,
            .biXPelsPerMeter = XPPM,
            .biYPelsPerMeter = YPPM,
            .biClrUsed = CLRUSET,
            .biClrImportant = IMPORTANT,
    };
    Header.biSizeImage = Header.bfileSize - Header.bOffBits;
    return Header;
}



static enum bmp_read_status check_header_status(const struct header header ){


    if (header.bfType != 0x4d42) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    if (header.bfileSize != header.bOffBits + header.biSizeImage || header.biCompression != 0 ||
        header.biPlanes != 1 || header.biSize != 40) {
        return READ_INVALID_HEADER;
    }

    if (header.biPlanes != 1) {
        return READ_INVALID_SIZE;
    }
    return READ_OK;
}

enum bmp_read_status from_bmp(FILE* in, struct image* img) {
    struct header bmp_header;
    size_t size = fread(&bmp_header, 1, sizeof(struct header), in);

    if(size != sizeof (struct header)) {
        return READ_INVALID_HEADER;
    }


    enum bmp_read_status header_check_status = check_header_status(bmp_header);
    if (header_check_status != READ_OK) {
        return header_check_status;
    }


    initialize_img(img, bmp_header.biWidth,  bmp_header.biHeight);


    for (size_t i = 0; i < img->height; i=i+1) {
        fread(&(img->data[i*img->width]), sizeof(struct pixel), img->width, in);
        fseek(in, padding(img), SEEK_CUR);
    }

    return READ_OK;
}

enum bmp_write_status to_bmp(FILE* out, struct image const* img) {
    if(out == NULL) return WRITE_INVALID_INPUT;
    struct header bmp_header = bmp_header_set(img);

    bmp_header.biHeight = img->height;
    bmp_header.biWidth = img->width;

    fwrite(&bmp_header, sizeof(struct header), 1, out);


    const uint64_t zero = 0;
    for(size_t i = 0; i < img->height; i=i+1) {
        fwrite(&(img->data[i * img->width]), sizeof(struct pixel), img->width, out);
        fwrite(&zero, 1, padding(img), out);
    }
    return WRITE_OK;
}

static char* const ReadError[] = {
        [READ_OK] = "bmp  was read\n",
        [READ_INVALID_SIGNATURE] = " Check signature(format file) \n",
        [READ_INVALID_BITS] = "Check bits(24-bit needs)\n",
        [READ_INVALID_HEADER] = "Check header's arguments \n",
        [READ_INVALID_SIZE] = "Check file's format\n"
};

enum bmp_read_status print_ReadError(enum bmp_read_status status) {
    printf("%s", ReadError[status]);
    return status;
}

static char* const WriteError[] = {
        [WRITE_OK] = "bmp was write\n",
        [WRITE_INVALID_INPUT] = "Check input\n",
        [WRITE_ERROR] = "Something is wrong\n"
};

enum bmp_write_status print_WriteError(enum bmp_write_status status) {
    printf("%s", WriteError[status]);
    return status;
}


