//
// Created by Maxim on 29.12.2021.
//
#include "file.h"
#include <stdio.h>

enum bmp_Read_Status read_bmp (FILE** file, const char* path) {
    *file = fopen(path, "rb");
    if (!*file){
        return FILE_READ_ERROR;
    }else{
        return FILE_READ_OK;
    }
}

static char* const file_read_image_status_decoder[] = {
        [FILE_READ_OK] = "input file  successfully open \n",
        [FILE_READ_ERROR] = "Check input file\n"
};

enum bmp_Read_Status print_bmpReadStatus(enum bmp_Read_Status status) {
    printf("%s", file_read_image_status_decoder[status]);
    return status;
}



enum bmp_Write_Status write_bmp (FILE** file, const char* path) {
    *file = fopen(path, "wb");
    if (!*file){
        return FILE_WRITE_ERROR;
    }else{
        return FILE_WRITE_OK;
    }
}

static char* const file_write_image_status_decoder[] = {
        [FILE_WRITE_OK] = "output file successfully open\n",
        [FILE_WRITE_ERROR] = "Check output file\n"
};

enum bmp_Write_Status print_bmpWriteStatus(enum bmp_Write_Status status) {
    printf("%s", file_write_image_status_decoder[status]);
    return status;
}



enum bmp_Close_Status close_bmp (FILE** file) {
    if (!*file) {
        return FILE_CLOSE_ERROR;
    }
    fclose(*file);
    return FILE_CLOSE_OK;
}

static char* const file_close_image_status_decoder[] = {
        [FILE_CLOSE_OK] = "file was closed\n",
        [FILE_CLOSE_ERROR] = "Try again\n"
};

enum bmp_Close_Status print_file_close_image_status(enum bmp_Close_Status status) {
    printf("%s", file_close_image_status_decoder[status]);
    return status;
}



