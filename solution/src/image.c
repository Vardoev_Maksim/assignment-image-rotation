//
// Created by Maxim on 29.12.2021.
//

#include "image.h"
#include <malloc.h>

void initialize_img(struct image* img, uint64_t width, uint64_t height) {
    uint64_t data_size = width * sizeof(struct pixel) *height;
    img->width = width;
    img->data = (struct pixel*) malloc(data_size);
    img->height = height;

}

void free_d(struct image *img) {
    free(img->data);
}


