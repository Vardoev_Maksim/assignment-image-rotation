#include "image.h"
#include "bmp.h"
#include "file.h"
#include "rotate.h"
#include <stdio.h>

int main(int argc, char **argv) {
    (void) argc;
     (void) argv;

     if (argc != 3) {
         printf("Check input format\n");
         return 1;
     }


    struct image image = {0};


    FILE *in = NULL;


    if (print_bmpReadStatus(read_bmp(&in, argv[1]))) return 1;
    if (print_ReadError(from_bmp(in, &image))) return 2;
    if (print_file_close_image_status(close_bmp(&in))) return 3;



    struct image rotated = rotate(&image);

    free_d(&image);

    FILE *out = NULL;
    if (print_bmpWriteStatus(write_bmp(&out, argv[2]))) return 4;
    if (print_WriteError(to_bmp(out, &rotated))) return 5;
    if (print_file_close_image_status(close_bmp(&out))) return 6;


    free_d(&rotated);

    return 0;
}

