//
// Created by Maxim on 29.12.2021.
//

#include "rotate.h"
#include "image.h"
#include <stdlib.h>

    struct image rotate(struct image const* image) {
        struct image ret = {0};
        ret.width = image->height;
        ret.data = malloc(sizeof(struct pixel) * image->width * image->height);
        ret.height = image->width;
        for (size_t row = 0; row < image->width;row=row+1) {
            for (size_t col = 0; col < image->height; col=col+1) {
                ret.data[row * ret.width + col] =  (image->data[row+ image->width*(image->height - 1 - col) ]);
            }
        }
        return ret;
    }


